const api = require("express").Router()

const AuthController = require('../controllers/api/authController')
const UserController = require('../controllers/api/userController')
const authController = new AuthController()
const userController = new UserController()
const bodyParser = require('body-parser')
const Restrict = require('../middlewares/restrict-api')

api.use(bodyParser.json())

api.post('/register', authController.register)
api.post('/login', authController.login)

//api.use(Restrict)

api.post('/logout', authController.logout)
api.delete('/:id', authController.delete)

// whoamiApi
//api.get("/whoamiApi", authApiContoller.whoamiApi);

//getuser
api.get('/', userController.getUser)
api.get('/:id', userController.getDetailUser)
api.post('/insertbiodata', userController.insertBiodata)

//score
api.post('/insertscore', userController.insertScore)
api.get('/score', userController.getScore)
api.delete('/score/:id', userController.deleteScore)

// CREATE ROOM
api.post("/room", userController.createRoom);
// FIGHT ROOM
api.post("/fight/:room", userController.fightRoom);

module.exports = api