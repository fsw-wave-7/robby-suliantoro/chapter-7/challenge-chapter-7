const web = require("express").Router()

const AuthController = require('../controllers/web/authController')
const HomeController = require('../controllers/web/homeController')
const authController = new AuthController()
const homeController = new HomeController()
const bodyParser = require('body-parser')
const Restrict = require('../middlewares/restrict-api')

web.use(bodyParser.json())

//web.post('/register', authController.register)
web.get('/login', authController.login)
web.post('/loginAdmin', authController.doLogin)

web.use(Restrict)

//web.post('/logout', authController.logout)

//halaman dashboard
web.get('/home', homeController.getHome)
//halaman list admin
web.get('/listAdmin', homeController.listAdmin)
//halaman add admin
web.post('/save-admin', authController.saveAdmin)
web.get('/addAdmin', authController.add)
//halaman edit admin
web.get('/editAdmin/:id', authController.edit)
web.post('/editAdmin/:id', authController.update)
//halaman delete admin
web.get('/delete/:id', authController.delete)
//halaman list user
web.get('/', homeController.getUser)
//halaman score
web.get('/score', homeController.getScore)

// web.get('/:id', homeController.getDetailUser)
// web.post('insertscore', homeController.insertScore)
// web.delete('/score/:id', homeController.deleteScore)

module.exports = web