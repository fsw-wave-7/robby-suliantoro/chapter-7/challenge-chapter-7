const messages = {
	200: "Sukses",
	201: "Data berhasil disimpan",
};

function successResponse(res, code, data, meta = {}) {
	res.status(code).json({
		data,
		meta: {
			code,
			message: messages[code.toString()],
			...meta,
		},
	});
}

function resultFight(res, code, data, meta = {}) {
	res.status(code).json({
		data,
		meta: {
			code,
			message: messages[code.toString()],
			...meta,
		},
	});
}

module.exports = {successResponse};
