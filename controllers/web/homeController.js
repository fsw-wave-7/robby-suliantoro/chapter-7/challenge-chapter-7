const { Admin, History, User, Biodata } = require("../../models")

class HomeController {
    listAdmin = (req,res) => {
        Admin.findAll()
        .then((admin) => {
            res.render('index', {
                judul: 'List Admin',
                content: './content/listAdmin',
                admin
            })
        })
    }
    
    // addAdmin = (req,res) => {
    //     Admin.create({
            
    // };

    getHome = (req,res) => {
        User.findAll()
        .then(() => {
            res.render('index', {
                judul: 'Dashboard',
                content: './content/home'
            })
        })
    }

    getUser = (req,res) => {
        User.findAll({include:[
            {
                model: Biodata,
                as : 'biodata'
            }
        ]}).then((user) => {
            res.render('index', {
                judul: 'List User',
                content: './content/listUser',
                user
            })
            //res.json(user[0].biodata.name)
        })
    }

    getScore = (req,res) => {
        History.findAll({include:[
            {
                model: User,
                as : 'user'
            }
        ],
        order: [['createdAt', 'DESC']]    
    }).then((history) => {
            res.render('index', {
                judul: 'Score',
                content: './content/score',
                history
            })
            //res.json(history)
        })
    }

    deleteScore = (req,res) => {
        History.destroy({
            where: {id: req.params.id}
        }).then(()=> {
            res.send('berhasil dihapus')
        })
    }
}

module.exports = HomeController;