const { Admin } = require("../../models")
const bcrypt = require("bcrypt")
const passport = require("../../lib/passport-jwt");

function format(user) { 
    const { id, username } = user 
    return {id, username, accessToken: user.generateToken()} 
  }

class AuthController {
    // register = async (req, res) => {
    //     User.register(req.body)
    //     .then(() => {
    //         res.send("data berhasil disimpan");
    //     })
    //     .catch((err) => res.send(err.message));
    // }

    login = (req,res) => {
        res.render('login', {
            judul: 'halaman login',
            content: './content/login'
        })
    }

    doLogin = 
    // passport.authenticate("local", {
	// 	successRedirect: "/home",
	// 	failureRedirect: "/login",
	// 	failureFlash: true,
	// });
    (req,res) => {
        Admin.authenticate(req.body)
        .then((admin) => {
          res.json(format(admin)) 
        })
        .then(() => {
            res.redirect('/home')
        })
        .catch((err) => res.send(err));
    }

    logout = (req,res) => {
        res.clearCookies('loginData')
        res.redirect('/login')
    }

    add = (req, res) => {
        res.render('index', {
            judul: 'Halaman Tambah Admin',
            content: './content/addAdmin'
        })
    }

    saveAdmin = async (req, res) => {

        const salt = await bcrypt.genSalt(10)

        Admin.create({
            username: req.body.username,
            password: await bcrypt.hash(req.body.password, salt)
        })
        .then(() => {
            res.redirect('/listAdmin')
        }) .catch(err => {
            console.log(err)
        })

    }

    edit = (req, res) => {

        const index = req.params.id;
        //SEQUELIZE CODE
        Admin.findOne({
            where: {id: index},
        }).then((admin) => {
        res.render('index', {
            judul: 'Halaman Edit Admin',
            content: './content/editAdmin',
            admin: admin,
        })

    })
    }

    update = async (req, res) => {

        const salt = await bcrypt.genSalt(10)

        Admin.update({
            username: req.body.username,
            password: await bcrypt.hash(req.body.password, salt)
        }, {
        where: { id: req.params.id }
        })
        .then(admin => {
            res.redirect('/listAdmin')
        }) .catch(err => {
            res.status(422).json("Can't update user")
        })
    }
    
    delete = (req,res) => {
        Admin.destroy({
            where: {id: req.params.id}
        }).then(()=> {
            res.redirect('/listAdmin')
            res.send('berhasil dihapus')
        })
    }
}

module.exports = AuthController;