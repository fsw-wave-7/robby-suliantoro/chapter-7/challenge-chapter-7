const { User, History, Biodata } = require("../../models")
const { successResponse} = require ("../../helper/response")

class UserController {
    getUser = (req,res) => {
        User.findAll().then((user) => {
            res.send(user)
        })
    }
    
    getDetailUser = (req,res) => {
        User.findOne({
            where: { id: req.params.id},
            include: [
                {
                    model: History,
                    as: 'history',
                },
                {
                    model: Biodata,
                    as : 'biodata'
                }
            ],
        }).then((user) => {
            res.send(user)
        });
    };

    insertBiodata = (req,res) => {
        Biodata.create({
            user_id: req.body.user_id,
            name: req.body.name,
            age: req.body.age
        }).then(() => {
            res.send("berhasil posting")
        })
    } 

    insertScore = (req,res) => {
        History.create({
            score: req.body.score,
            user_id: req.body.user_id,
            waktu_bermain: req.body.waktu_bermain,
            room_id: req.body.room_id
        }).then(() => {
            res.send("berhasil posting")
        })
    }

    getScore = (req,res) => {
        History.findAll()
            .then((history) => {
            // res.render('index', {
            //     judul: 'Score',
            //     content: './content/score',
            //     history
            // })
            
            
            //res.json(history)

            res.send(history);

        })
        .catch((err) => console.log(err))
    }

    deleteScore = (req,res) => {
        History.destroy({
            where: {id: req.params.id}
        }).then(()=> {
            res.send('berhasil dihapus')
        })
    }

    createRoom = (req, res) => {
		const room_id = req.body.room_id;
		const user_id = req.body.user_id;
		History.create({
			room_id,
			user_id,
		}).then((data) => {
			successResponse(res, 201, data);
		});
	};

	fightRoom = (req, res) => {
		const room_id = req.params.room;
		let data1 = {
			p1: req.body.first_player_id,
			p1Choice: req.body.first_player_choice,
		};

		let data2 = {
			p2: req.body.second_player_id,
			p2Choice: req.body.second_player_choice,
		};

		let result = [];

		result.push({data1, data2});

		console.log(result);

		// History.update(
		// 	{
		// 		score
		// 	},
		// 	{where: {room_id}}
		// ).then((data) => {
		// 	successResponse(res, 201, result);
		// });
	};
}

module.exports = UserController;