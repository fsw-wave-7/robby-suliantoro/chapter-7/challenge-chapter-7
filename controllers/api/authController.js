const { User } = require("../../models")
const bcrypt = require("bcrypt")
const passport = require("../../lib/passport-jwt");

function format(user) { 
    const { id, username } = user 
    return {id, username, accessToken: user.generateToken()} 
  }

class AuthController {
    register = async (req, res) => {
        User.register(req.body)
        .then(() => {
            res.status(200);
            res.send("data berhasil disimpan");
        })
        .catch((err) => res.send(err.message));
    }

    login = (req,res) => {
        User.authenticate(req.body)
        .then((user) => {
            res.status(200);
            res.json(format(user)) 
        })
        .catch((err) => res.send(err));
    }

    logout = (req,res) => {
        res.clearCookies('loginData')
        res.redirect('/login')
    }

    delete = (req,res) => {
        User.destroy({
            where: {id: req.params.id}
        }).then(()=> {
            res.status(200);
            res.send('berhasil dihapus');
        })
    }

    whoamiApi = (req,res) => {
        const currentAdmin = req.body;
        res.json(currentAdmin);
    };
}

module.exports = AuthController;