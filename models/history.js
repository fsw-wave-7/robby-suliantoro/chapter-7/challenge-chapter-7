'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      History.belongsTo(models.User, {
        as : "user",
        foreignKey: "user_id",
        onDelete : "CASCADE"
      })
      // define association here
    }
  };
  History.init({
    user_id: DataTypes.INTEGER,
		room_id: DataTypes.INTEGER,
    score: DataTypes.INTEGER,
    waktu_bermain: DataTypes.INTEGER,
    room_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'History',
  });
  return History;
};